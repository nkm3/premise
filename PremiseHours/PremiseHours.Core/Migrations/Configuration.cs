﻿/*
 * Created By Nathan Menkveld for Premise code test
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using PremiseHours.Core.Entities;
using PremiseHours.Core.Repositories;

namespace PremiseHours.Core.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<PremiseHoursContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        bool _updateHours = true; // In a real project, I would put this in a web transform

        protected override void Seed(PremiseHoursContext context)
        {
            base.Seed(context);

            #region Users

            context.Users.AddOrUpdate(
                x => x.Email,
                new User()
                {
                    FirstName = "Hans",
                    LastName = "Zimmer",
                    Email = "HZ@User.com",
                    Role = Role.User
                },
                new User()
                {
                    FirstName = "James ",
                    LastName = "Horner",
                    Email = "JH@User.com",
                    Role = Role.User
                },
                new User()
                {
                    FirstName = "John ",
                    LastName = "Williams",
                    Email = "JW@Admin.com",
                    Role = Role.Admin
                }
            );
            context.SaveChanges();
            User HansZimmer = context.Users.Single(x => x.Email == "HZ@User.com");
            User JamesHorner = context.Users.Single(x => x.Email == "JH@User.com");
            User JohnWilliams = context.Users.Single(x => x.Email == "JW@Admin.com");

            #endregion

            #region Shifts
            if (_updateHours)
            {
                context.Shifts.AddOrUpdate(
                    x => new { x.User, x.StartDate },
                    // Hans Zimmer
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-30).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-30).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-28).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-28).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-27).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-27).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-25).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-25).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-24).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-24).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-23).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-23).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-22).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-22).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-21).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-21).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-19).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-19).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-18).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-18).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-17).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-17).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-15).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-15).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-14).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-14).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-13).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-13).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-12).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-12).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-11).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-11).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-10).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-10).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-8).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-8).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-7).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-7).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-5).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-5).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-4).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-4).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-3).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-3).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-2).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-2).AddHours(20),
                        UserId = HansZimmer.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-1).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-1).AddHours(20),
                        UserId = HansZimmer.Id
                    },

                    // James Horner
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-30).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-30).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-28).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-28).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-27).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-27).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-25).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-25).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-24).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-24).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-23).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-23).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-22).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-22).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-21).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-21).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-19).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-19).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-18).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-18).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-17).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-17).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-15).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-15).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-14).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-14).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-13).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-13).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-12).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-12).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-11).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-11).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift() // Note: this one spans multiple days
                    {
                        StartDate = DateTime.Today.AddDays(-10).AddHours(20),
                        EndDate = DateTime.Today.AddDays(-9).AddHours(6),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-8).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-8).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-7).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-7).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-5).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-5).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-4).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-4).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-3).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-3).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-2).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-2).AddHours(20),
                        UserId = JamesHorner.Id
                    },
                    new Shift()
                    {
                        StartDate = DateTime.Today.AddDays(-1).AddHours(8),
                        EndDate = DateTime.Today.AddDays(-1).AddHours(20),
                        UserId = JamesHorner.Id
                    }
                );
                context.SaveChanges();
            }

            #endregion
        }

    }
}
