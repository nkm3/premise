namespace PremiseHours.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddShiftsAndUpdateUsers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Shifts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        UserId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            AddColumn("dbo.Users", "Role", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Shifts", "UserId", "dbo.Users");
            DropIndex("dbo.Shifts", new[] { "UserId" });
            DropColumn("dbo.Users", "Role");
            DropTable("dbo.Shifts");
        }
    }
}
