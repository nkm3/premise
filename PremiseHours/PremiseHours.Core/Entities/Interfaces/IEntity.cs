﻿/*
 * Created By Nathan Menkveld for Premise code test
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PremiseHours.Core.Entities.Interfaces
{
    public class IEntity
    {
        long Id { get; set; }

        // Skipping adding things like audit fields, which I would usually put in here
    }
}
