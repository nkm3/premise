﻿/*
 * Created By Nathan Menkveld for Premise code test
 */

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PremiseHours.Core.Entities
{
    /*
     * Mocking out roles with an enum to just so the UI can key off user roles without me actually building that out right now
     */
    public enum Role
    {
        [Display(Name = "User")]
        User,
        [Display(Name = "Administrator")]
        Admin
    }
}
