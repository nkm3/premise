﻿/*
 * Created By Nathan Menkveld for Premise code test
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PremiseHours.Core.Entities.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace PremiseHours.Core.Entities
{
    public class BaseEntity : IEntity
    {
        [Key]
        public virtual long Id { get; set; }
    }
}
