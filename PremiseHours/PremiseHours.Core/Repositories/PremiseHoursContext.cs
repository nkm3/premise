﻿/*
 * Created By Nathan Menkveld for Premise code test
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using PremiseHours.Core.Entities;

namespace PremiseHours.Core.Repositories
{
    public class PremiseHoursContext : DbContext
    {

        #region DBSets
        public DbSet<User> Users { get; set; }
        public DbSet<Shift> Shifts { get; set; }
        #endregion

        public PremiseHoursContext() : base("PremiseHourseDB")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>(); // Can selectively be turned back on for specific relationsips as needed
            
        }
    }
}
