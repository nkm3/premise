﻿using System.Web;
using System.Web.Optimization;

namespace PremiseHours
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            #region Written by Nathan Menkveld

            bundles.Add(new ScriptBundle("~/bundles/moment").Include(
                     "~/Scripts/moment.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                    
                     "~/Scripts/fullcalendar.js",
                     "~/Scripts/custom/*.js"));

            #endregion

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/fullcalendar.css",
                      "~/Content/bootstrap.css",
                      "~/Content/bootswatch.css", //NKM -  I added this, which imports a different base theme for the UI
                      "~/Content/site.css"));
        
        }
    }
}
