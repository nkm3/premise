﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PremiseHours.Startup))]
namespace PremiseHours
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
