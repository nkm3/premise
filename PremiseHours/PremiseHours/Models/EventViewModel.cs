﻿/*
 * Created By Nathan Menkveld for Premise code test
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PremiseHours.Models
{
    public class EventViewModel
    {
        // These are lowercase so they will work for fullcalendar without me having to call toLower() right now
        public int id { get; set; }
        public DateTime? start { get; set; }
        public DateTime? end { get; set; }
        public string title { get; set; }
        public bool allDay { get; set; }
    }
}