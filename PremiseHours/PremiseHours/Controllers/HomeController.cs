﻿/*
 * Created By Nathan Menkveld for Premise code test
 */
using PremiseHours.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using PremiseHours.Core.Repositories;
using System.Data.Entity;

namespace PremiseHours.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> GetCalendarEvents(long userId)
        {
            // running out of time, so not putting this down in Core.services / using a transmission layer
            using (var dbContext = new PremiseHoursContext())
            {
                var query = from s in dbContext.Shifts
                            where s.UserId == userId
                            select new
                            {
                                id = s.Id,
                                allDay = false,
                                start = s.StartDate,
                                end = s.EndDate,
                                title = "Work"
                            };

                var entities = await query.ToListAsync();
                return Json(entities, JsonRequestBehavior.AllowGet);
            }
        }
    }
}